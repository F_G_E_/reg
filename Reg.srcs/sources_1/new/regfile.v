`timescale 1ns / 1ps
//*************************************************************************
//   > 文件名: regfile.v
//   > 描述  ：寄存器堆模块，同步写，异步读
//   > 作者  : LOONGSON
//   > 日期  : 2016-04-14
//*************************************************************************
module regfile(
    input             clk,
    input      [3 :0] wen,//写使能端，用于控制4个字节的写操作
    input      [1 :0] ren,//读使能端，用于控制读操作的前16位与后16位
    input      [4 :0] raddr1,
    input      [4 :0] raddr2,
    input      [4 :0] waddr,
    input      [31:0] wdata,
    output reg [31:0] rdata1,
    output reg [31:0] rdata2,
    input      [4 :0] test_addr,
    output reg [31:0] test_data
    );
    reg [31:0] rf[31:0];
     
    // three ported register file
    // read two ports combinationally
    // write third port on rising edge of clock
    // register 0 hardwired to 0

    always @(posedge clk)
    begin
        //当wen[0]为1时，写入最低位的字节（即第0--7位）
        if (wen[0] == 1'b1) 
        begin
            rf[waddr][7:0] <= wdata[7:0];
        end
        //当wen[1]为1时，写入第8--15位
        if(wen[1] == 1'b1)
        begin
            rf[waddr][15:8] <= wdata[15:8];
        end
        //当wen[2]为1时，写入第16--23位
        if(wen[2] == 1'b1)
        begin
            rf[waddr][23:16] <= wdata[23:16];
        end
        //当wen[3]为1时，写入24--31位
        if(wen[3] == 1'b1)
        begin
            rf[waddr][31:24] <= wdata[31:24];
        end
    end
     
    //读端口1
    always @(*)
    begin
        case (raddr1)
            //0号寄存器的数值永远为0
            5'd0 : rdata1 <= 32'd0;
            //将raddr1号寄存器中的值赋给rdata1
            default: 
                //当ren==00时，不进行读操作
                rdata1 = (ren[1]==0 && ren[0]==0)? rdata1:
                //当ren==01时，读入低16位
                (ren[1]==0 && ren[0]==1)? {rdata1[31:16],rf[raddr1][15:0]}:
                //当ren==10时，读入高16位，当ren==11时，读入低16位于高16位
                (ren[1]==1 && ren[0]==0)? {rf[raddr1][31:16],rdata1[15:0]}: rf[raddr1];
        endcase
    end
    //读端口2
    always @(*)
    begin
        case (raddr2)
            //0号寄存器的值永远为0
            5'd0 : rdata2 <= 32'd0;
            //将raddr2号寄存器中的值赋给rdata2
            default: 
                //当ren==00时，不进行读操作
                rdata2 = (ren[1]==0 && ren[0]==0)? rdata2:
                //当ren==01时，读入低16位
                (ren[1]==0 && ren[0]==1)? {rdata2[31:16],rf[raddr2][15:0]}:
                //当ren==10时，读入高16位，当ren==11时，读入低16位于高16位
                (ren[1]==1 && ren[0]==0)? {rf[raddr2][31:16],rdata2[15:0]}:rf[raddr2];
        endcase
    end
     //调试端口，读出寄存器值显示在触摸屏上
    always @(*)
    begin
        case (test_addr)
            //0号寄存器的数值永远为0
            5'd0 : test_data <= 32'd0;
            //将test_addr号寄存器中的值赋给test_data
            default: test_data <= rf[test_addr];
        endcase
    end
endmodule
